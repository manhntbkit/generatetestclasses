public with sharing class GenerateDFPageController {
    public string recordId {get;set;}
    public string objectName {get;set;}
    public string objectAPI {get;set;}
    public string ApexClassTest {get;set;}
    
    
    public GenerateDFPageController(){
        recordId = null;
        objectName = '';
        objectAPI = '';
    }
    public void getMetadata(){
        if(string.isBlank(recordId)) return;
        objectAPI = ID.valueOf(recordId).getSObjectType().getDescribe().getName();
        objectName = ID.valueOf(recordId).getSObjectType().getDescribe().getLabel();
        set<string> generateRelationship = new set<string>();
        ObjectRecordDescription firstTest = ObjectRecordDescription.generateObjectRecord(recordId, objectName.replace(' ','').replace('__',''), objectAPI,generateRelationship,true);
        list<String> listClass = new list<String>();
        map<id,string> mapRecordID = new map<id,string>();
        ObjectRecordDescription.generateDataFactory(firstTest, listClass,mapRecordID);
        ApexClassTest = '';
        for(String s: listClass){
            ApexClassTest += s + '\n\n\n';
        }
    }
}