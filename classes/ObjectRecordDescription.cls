/*
    Le Nguyen: leeseifer@gmail.com
    18/10/2016
    
*/

public with sharing class ObjectRecordDescription {
    public ID recordID {get;set;}
    public string objectName {get;set;}
    public string objectAPI {get;set;}
    public list<Sobject> actualRecord {get;set;}
    public map<string, Schema.DisplayType> updatableFields {get;set;}
    public map<string,Schema.DescribeSObjectResult> referenceObjects {get;set;}
    public map<string,string> referenceRelationshipName {get;set;}
    public list<ObjectRecordDescription> relatedRecords {get;set;}
    public set<string> genRelationships {get;set;}

    public ObjectRecordDescription(){
        this.recordID = null;
        this.objectName = '';
        this.objectAPI = '';
        this.updatableFields = new  map<string, Schema.DisplayType>();
        this.referenceObjects = new map<string,Schema.DescribeSObjectResult>();
        this.referenceRelationshipName = new map<string,string>();
        this.relatedRecords = new list<ObjectRecordDescription>();
        this.genRelationships = new set<string>();
    }



    //Dynamic SOQL to get data from selected ID
    public string generateSOQL(){
        string rtn = 'select ';
        for(string s: updatableFields.keyset()){
            rtn += s+',';
        }
        rtn = rtn.left(rtn.length()-1) + ' ';
        rtn += 'from '+ this.objectAPI + ' where Id=\''+this.recordID+'\'';

        system.debug(rtn);
        return rtn;
    }

    //Loop fields and relationship of giving object name / ID
    public static ObjectRecordDescription generateObjectRecord(Id recID, string objectName, string objectAPI,set<string> generateRelationship, boolean isGetRelated){
        ObjectRecordDescription rtn = new ObjectRecordDescription();
        rtn.recordID = recID;
        rtn.objectName = objectName;
        rtn.objectAPI = objectAPI;
        SObjectType objType = Schema.getGlobalDescribe().get(objectAPI);
        Map<String,Schema.SObjectField> mfields = objType.getDescribe().fields.getMap();
        for(Schema.SObjectField mfield:mfields.values()){
            Schema.DescribeFieldResult DFR= mfield.getDescribe();
            //We can put of a list of ignore standard field name here exm: OwnerID
            if(DFR.isCreateable() && DFR.getName() != 'OwnerId'){
                rtn.updatableFields.put(DFR.getName(),DFR.getType());
                if(DFR.getType() == Schema.DisplayType.REFERENCE){
                    for(Schema.sObjectType sOT: DFR.getReferenceTo()){
                        if(sOT.getDescribe().getName() != 'User'&& !string.isBlank(DFR.getRelationshipName())){ //Ignore User Type, we will not try to generate User data
                            rtn.referenceObjects.put(DFR.getName(),sOT.getDescribe());
                            rtn.referenceRelationshipName.put(DFR.getName(),DFR.getRelationshipName().replace(' ','').replace('__',''));
                        }
                        
                    } 
                }
            }
        }
        rtn.actualRecord = Database.query(rtn.generateSOQL()); // get real data

        //Check if we going to generate all relationships.
        if(isGetRelated){
            for(String objName: rtn.referenceObjects.keySet()){
                id relatedRecordID = (ID)rtn.actualRecord[0].get(objName);
                // Using RelationShip Name to ignore recursive type like Contact->Accont->Contact
                if(!generateRelationship.contains(rtn.referenceRelationshipName.get(objName)) && relatedRecordID != null){ 
                    generateRelationship.add(rtn.referenceRelationshipName.get(objName));
                    rtn.genRelationships.add(objName);
                    Schema.DescribeSObjectResult tempObject = rtn.referenceObjects.get(objName);
                    string relatedObjectName = rtn.referenceRelationshipName.get(objName);
                    string relatedObjectAPI = tempObject.getName();
                    rtn.relatedRecords.add(generateObjectRecord(relatedRecordID,relatedObjectName,relatedObjectAPI,generateRelationship,isGetRelated));
                }
            }
        }
        return rtn;
    }



    //Generate static classes to create create new Method.
    public static void generateDataFactory(ObjectRecordDescription objDescription, list<string> classList,map<id,string> mapRecordID){
        mapRecordID.put(objDescription.recordID,'create'+objDescription.objectName+'().Id');
        string rtn = 'public static ' + objDescription.objectAPI + ' create'+objDescription.objectName+'(){\n\t';
        string newObjectName = ' createNew'+objDescription.objectName;

        rtn += objDescription.objectAPI + newObjectName+ ' = new '+objDescription.objectAPI+'();\n\t';
        for (String s: objDescription.updatableFields.keyset()){

            //Manh updated 24Apr.
            Schema.DescribeSObjectResult tempObject = Schema.getGlobalDescribe().get(objDescription.objectAPI).getDescribe();
            Schema.DescribeFieldResult field = tempObject.fields.getMap().get(s).getDescribe();

            object defaultValue = field.getDefaultValue();
            object actualValue = objDescription.actualRecord[0].get(s);

            if(defaultValue !=  actualValue){
                rtn += generateDataCode(newObjectName,s,objDescription.updatableFields.get(s),objDescription.actualRecord[0],objDescription.referenceRelationshipName,objDescription.genRelationships);
            }
            //

            
        }
        rtn += 'insert'+newObjectName+';\n\t';
        rtn += 'return'+newObjectName+';\n}';
        for(ObjectRecordDescription objDesc: objDescription.relatedRecords){
            if(!mapRecordID.containsKey(objDesc.recordID))
                generateDataFactory(objDesc,classList,mapRecordID);
        }

        for(string oId :  mapRecordID.keyset()){
            rtn = rtn.replace(oid,mapRecordID.get(oID));
        }

        classList.add(rtn);
    }

    //generate Body Code
    private static String generateDataCode(string newObjectName,string fieldAPI, Schema.DisplayType fieldType, SObject sObj, map<string,string> referenceRelationshipName, set<String> refs){   
        string combineName = newObjectName + '.'+fieldAPI;
        if(fieldType == Schema.DisplayType.String || 
            fieldType == Schema.DisplayType.TextArea ||
            fieldType == Schema.DisplayType.Id ||
            fieldType == Schema.DisplayType.Picklist ||
            fieldType == Schema.DisplayType.Phone ||
            fieldType == Schema.DisplayType.Email ||
            fieldType == Schema.DisplayType.MultiPicklist ||
            fieldType == Schema.DisplayType.URL){
                string rtn = (String)sObj.get(fieldAPI);
                if(string.IsBlank(rtn)){
                    return '';   
                }
                return combineName + ' = \''+(String)sObj.get(fieldAPI)+'\' ;\n\t'; 
            }
        if(fieldType == Schema.DisplayType.Currency ||
           fieldType == Schema.DisplayType.Double){
            if(sObj.get(fieldAPI)!=null)
            return combineName + ' = '+String.ValueOf(sObj.get(fieldAPI))+' ;\n\t';
        }
            
        if(fieldType == Schema.DisplayType.Integer){
            if(sObj.get(fieldAPI)!=null)
            return combineName + ' = '+String.ValueOf(sObj.get(fieldAPI))+' ;\n\t';
        }
        if(fieldType == Schema.DisplayType.Boolean)
            return combineName + ' = '+String.ValueOf(sObj.get(fieldAPI))+' ;\n\t';
        if(fieldType == Schema.DisplayType.DateTime){
            DateTime rtn = (DateTime)sObj.get(fieldAPI);
            if(rtn == null){
                return '';   
            }
            return combineName + ' = (DateTime)JSON.deserialize(\''+JSON.serialize(rtn)+'\',DateTime.Class) ;\n\t';
        }
        if(fieldType == Schema.DisplayType.Date){
            Date rtn = (Date)sObj.get(fieldAPI);
            if(rtn == null){
                return '';   
            }
            return combineName + ' = (Date)JSON.deserialize(\''+JSON.serialize(rtn)+'\',Date.Class) ;\n\t';
        }
        if(fieldType == Schema.DisplayType.Combobox)
            return combineName + ' = '+String.ValueOf(sObj.get(fieldAPI))+' ;\n\t';

        if(fieldType == Schema.DisplayType.REFERENCE){
            if(!refs.contains(fieldAPI)){
                return combineName + ' = null;\n\t';
            }
            return combineName + ' = '+String.ValueOf(sObj.get(fieldAPI))+' ;\n\t';

        }
        return '';   
    }
}