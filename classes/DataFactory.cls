@istest
public class DataFactory {
    public static Account createAccount(){
        Account createNewAccount = new Account();
        createNewAccount.Name = 'Smith Enterprises' ;
        createNewAccount.Type = 'Prospect' ;
        createNewAccount.ParentId = null;
        createNewAccount.Industry = 'Energy' ;
        insert createNewAccount;
        return createNewAccount;
    }


    public static Contact createContact(){
        Contact createNewContact = new Contact();
        createNewContact.AccountId = createAccount().Id ;
        createNewContact.LastName = 'Nguyen' ;
        createNewContact.FirstName = 'Le' ;
        createNewContact.ReportsToId = null;
        insert createNewContact;
        return createNewContact;
    }

}